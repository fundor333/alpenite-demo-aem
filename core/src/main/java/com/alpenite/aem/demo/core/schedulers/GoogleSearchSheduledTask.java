/*
 *  Copyright 2015 Adobe Systems Incorporated
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.alpenite.aem.demo.core.schedulers;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.Designate;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.net.ssl.HttpsURLConnection;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;


/**
 * A simple demo for cron-job like tasks that get executed regularly.
 * It also demonstrates how property values can be set. Users can
 * set the property values in /system/console/configMgr
 */
@Designate(ocd = GoogleSearchSheduledTask.Config.class)
@Component(service = Runnable.class)
public class GoogleSearchSheduledTask implements Runnable {

    @ObjectClassDefinition(name = "Google Search Task",
            description = "Ricerca e salva dati google")
    public static @interface Config {

        @AttributeDefinition(name = "Cron-job expression")
        String scheduler_expression() default "0 */10 * * * ?";

        @AttributeDefinition(name = "Concurrent task",
                description = "Whether or not to schedule this task concurrently")
        boolean scheduler_concurrent() default false;

        @AttributeDefinition(name = "Api Google", description = "Api di Google per la ricerca")
        String googleApiKey() default "";

        @AttributeDefinition(name = "Id search", description = "Id del motore di ricerca")
        String searchId() default "";

        @AttributeDefinition(name = "Query", description = "Query eseguita nella task")
        String query() default "";
    }

    private final Logger logger = LoggerFactory.getLogger(getClass());

    private String api_key;
    private String search_id;
    private String query;
    private String service_url;

    @Override
    public void run() {
        logger.info("GoogleSearchSheduledTask is now running, Api_key='" + api_key + "', Search='" + search_id + "', Query='" + query + "'");
        URL url;
        try {

            url = new URL(service_url);
            HttpsURLConnection con = (HttpsURLConnection) url.openConnection();
            String googleJson = getJson(con);
            logger.info(googleJson);

        } catch (MalformedURLException e) {
            e.printStackTrace();
            logger.error(e.getLocalizedMessage());
        } catch (IOException e) {
            e.printStackTrace();
            logger.error(e.getLocalizedMessage());
        }
    }

    private Map<String, String> getData(String json_data) {
        Map<String, String> output = new HashMap<String, String>();
        JSONParser parser = new JSONParser();
        try {
            JSONObject jsonObject = (JSONObject) parser.parse(json_data);
            JSONArray items = (JSONArray) jsonObject.get("items");

            for (Object item : items) {
                JSONObject single = (JSONObject) item;
                output.put((String) single.get("title"), (String) single.get("link"));
                logger.info(single.get("title") + " " + single.get("link"));
            }

        } catch (ParseException e) {
            e.printStackTrace();
            logger.error(e.getLocalizedMessage());
        }
        return output;
    }

    private String getJson(HttpsURLConnection con) {
        if (con != null) {
            try {
                BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream()));
                String input;
                StringBuilder output = new StringBuilder();
                while ((input = br.readLine()) != null) {
                    output.append(input);
                }
                br.close();
                return output.toString();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return "";
    }

    @Activate
    protected void activate(final Config config) {
        api_key = config.googleApiKey();
        search_id = config.searchId();
        query = config.query();
        service_url = "https://www.googleapis.com/customsearch/v1?key=" + api_key + "&cx=" + search_id + "&q=" + query;
    }

}
