package com.alpenite.aem.demo.core.servlets;

import com.alpenite.aem.demo.core.models.StudentModel;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import java.io.IOException;
import java.lang.reflect.Array;
import java.util.Iterator;

@Component(service = Servlet.class, property = {
        Constants.SERVICE_DESCRIPTION + "=Current Student A",
        "sling.servlet.methods=" + HttpConstants.METHOD_GET,
        "sling.servlet.paths=" + "/bin/studenti"
})

public class AllStudentServlet extends SlingSafeMethodsServlet {

    @Reference
    private ResourceResolverFactory resolverFactory;

    public void doGet(final SlingHttpServletRequest req, final SlingHttpServletResponse resp) throws ServletException, IOException {
        ResourceResolver resolver = req.getResourceResolver();
        resp.setContentType("text/plain");
        try {
            String resourcePath = "/etc/studenti";

            // ResourceResolver resourceResolver = resolverFactory.getAdministrativeResourceResolver(null);

            Resource res = resolver.getResource(resourcePath);

            // do something with the resource
            // when done, close the ResourceResolver

            Iterator<Resource> i = res.listChildren();
            int a = 0;
            while (i.hasNext()) {
                Resource element = i.next();
                StudentModel student = element.adaptTo(StudentModel.class);
                resp.getWriter().write("Studente"+ a++ + " = " + student.getName() + " " + student.getSurname()+"\n");
            }

            resolver.close();

        }
        catch (Exception e)
        {
            System.out.println("Error "+e.getLocalizedMessage());
        }
    }
}
