package com.alpenite.aem.demo.core.models;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.Model;

import javax.inject.Inject;
import javax.inject.Named;

@Model(adaptables = Resource.class)
public class TeacherModel extends HumanModel{

    @Inject
    @Named("hours")
    @Default(values = "0")
    protected double hours;

    public double getHours()
    {
        return hours;
    }
}
