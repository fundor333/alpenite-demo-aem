package com.alpenite.aem.demo.core.servlets;

import com.alpenite.aem.demo.core.models.StudentModel;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.*;
import org.apache.sling.api.servlets.HttpConstants;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import java.io.IOException;

@Component(service = Servlet.class, property = {
        Constants.SERVICE_DESCRIPTION + "=Current Student A",
        "sling.servlet.methods=" + HttpConstants.METHOD_GET,
        "sling.servlet.paths=" + "/bin/studenta"
})

public class StudentServlet extends SlingSafeMethodsServlet {

    @Reference
    private ResourceResolverFactory resolverFactory;

    public void doGet(final SlingHttpServletRequest req, final SlingHttpServletResponse resp) throws ServletException, IOException {
        ResourceResolver resolver = req.getResourceResolver();
        resp.setContentType("text/plain");
        String resourcePath = "/etc/studenti/studenteA";

        // ResourceResolver resourceResolver = resolverFactory.getAdministrativeResourceResolver(null);

        Resource res = resolver.getResource(resourcePath);

        // do something with the resource
        // when done, close the ResourceResolver

        StudentModel student = res.adaptTo(StudentModel.class);
        resp.getWriter().write("StudenteA = " + student.getName() + " " + student.getSurname());
        resolver.close();
    }
}
